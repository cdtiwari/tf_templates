output "vpc_id" {
  value = "${aws_vpc.tsi_vpc.id}"
}

output "subnet_id" {
  value = "${aws_subnet.tsi_subnet.id}"
}

output "vpc_sg_ids" {
  value = ["${aws_security_group.tsi_sg.id}"]
}
