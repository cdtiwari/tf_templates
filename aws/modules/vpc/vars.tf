variable "namespace" {
  default = "cd"
}

variable "vpc_name" {
  default = "vpc"
}

variable "vpc_id" {}

variable "vpc_cidr" {
  default = "10.0.0.0/18"
}

variable "destination_cidr_block" {
  default = "0.0.0.0/0"
}

variable "tenancy" {
  default     = "dedicated"
  description = "Name of the tenancy"
}

variable "cidr_blocks" {
  default = "0.0.0.0/0"
}

variable "subnet_name" {
  default = "subnet"
}

variable "subnet_cidr" {
  description = "CIDR of the desired subnet for the VPC, e.g. 10.0.0.0/8 or 192.168.0.0/16"
  default     = "10.0.1.0/18"
}

variable "map_public_ip_on_launch" {
  default = "true"
}

variable "aws_sg_name" {
  default = "tsi"
}
