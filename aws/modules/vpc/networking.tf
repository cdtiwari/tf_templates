/*
Module name: Networking
Author: C.D.Tiwari
Email: cdtiwari@ebi.ac.uk

Description: This module is rewponsible for creating below infrastructure

1. aws_vpc -> tsi_vpc
2. aws_subnet -> tsi_subnet
3. aws_sg -> tsi (default ssh, http, https are enabled)
4. aws_ig -> main
5. aws_rote -> internet_access
6. aws_rt -> tsi_rt
7. aws_rt_association -> tsi_rt_association
*/

# Create a VPC to launch our instances into
resource "aws_vpc" "tsi_vpc" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "${var.tenancy}"

  tags = {
    Name = "${var.namespace}-${var.vpc_name}"
  }
}

# Create a subnet to launch our instances into
resource "aws_subnet" "tsi_subnet" {
  vpc_id                  = "${aws_vpc.tsi_vpc.id}"
  cidr_block              = "${var.subnet_cidr}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  tags = {
    Name = "${var.namespace}-${var.subnet_name}"
  }
}

# Create a security group for the TSI so it is accessible via web
resource "aws_security_group" "tsi_sg" {
  name        = "${var.namespace}_${var.aws_sg_name}"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.tsi_vpc.id}"

  # SSH access from anywhere, allow port 22
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr_blocks}"]
  }

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr_blocks}"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port   = 442
    to_port     = 442
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr_blocks}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.cidr_blocks}"]
  }

  tags {
    Name = "${var.namespace}-tsi_sg"
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.tsi_vpc.id}"

  tags {
    Name = "${var.namespace}-tsi_gw"
  }
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.tsi_vpc.main_route_table_id}"
  gateway_id             = "${aws_internet_gateway.main.id}"
  destination_cidr_block = "${var.destination_cidr_block}"
}

# Create the Route Table
resource "aws_route_table" "tsi_rt" {
  vpc_id = "${aws_vpc.tsi_vpc.id}"

  tags {
    Name = "${var.namespace}-tsi_rt"
  }
} # end resource

# Associate the Route Table with the Subnet
resource "aws_route_table_association" "tsi_rt_association" {
  subnet_id      = "${aws_subnet.tsi_subnet.id}"
  route_table_id = "${aws_route_table.tsi_rt.id}"
} # end resource
