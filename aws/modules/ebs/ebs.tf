
resource "aws_ebs_volume" "tsi_ebs" {
  availability_zone = "${var.aws_region}c"
  size              = "${var.ebs_size}"
  encrypted = true
  tags = {
    Name = "${var.namespace}-tsi_ebs"
  }
//  encrypted         = "${var.is_encrypt}"
//    type              = "${var.ebs_type}"
//    size              = "${var.ebs_size}"
//    snapshot_id       = "${var.ebs_use_snap}"
//    availability_zone = "${var.aws_region}c"
//  tags {
//    Name            = "${var.namespace}"
//    Environment     = "${var.ebs_env}"
//    Application     = "${var.ebs_app}"
//    Tier            = "${var.ebs_tier}"
//    Role            = "${var.ebs_role}"
//    Resource        = "EBS${var.ebs_mp}"
//    Dev             = "${var.ebs_dev}"
//    MountPoint      = "${var.ebs_mp}"
//  }
}
