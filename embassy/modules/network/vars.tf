variable "namespace" {
  default = "CD"
}
variable "external_network" {

}

variable "network_name" {

}

variable "cluster_name" {

}

variable "subnet" {

}

variable "subnet_cidr" {

}

variable "use_neutron" {

}

variable "dns_nameservers" {
  type = "list"
}

variable "default_security_group" {
  description = "Default security group, optional"
  default = "default"
}