variable "namespace" {
  default = "cd"
  description = "Define a name space which will be reflected across the tenancy."
}

variable "external_network" {
  default = "ext-net-36"
  description = "Define external network"
}

variable "tenancy_user" {
  default = "centos"
}
//namespace = "cd"
//
//external_network = "ext-net-36"
//tenancy_user = "centos"
//
//bastion_image = "CentOS7-Cloud"
//bastion_disk_gb = 50
//bastion_flavour = "s1.medium"
//
//
//bastion_volumes = [
//  {
//    mount = "/home/user",
//    size = 200,
//    label = "USER",
//    export = "rw"
//  },
//  {
//    mount = "/data",
//    size = 750,
//    label = "DATA",
//    export = "rw",
//    mode = "3777",
//    group = "tsi"
//  },
//]
//
//host_image = "centos74"
//host_disk_gb = 60
//host_flavour = "s1.gargantuan"
//host_count = 2
//
//groups = [
//  { name = "tsi" }
//]
//
//users = [
//  {
//    user = "davidyuan",
//    id   = "8120",
//    sudo = "yes",
//    home = "/home/user/davidyuan",
//    groups = [ "tsi" ],
//    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbVJdgtiojWuTmIpa6GzIVeaapJKwMb3zG7Y8iWgkhFRziLK+SfRoeg4VigFOMhpDRuMFbK/5Ui61XJ/mDGNoKFX8Zr4CZ8f+e5nzZWY/w58p5s2g2cbJcpJV249qKmlnNKQJi+qONAoIczQN4Hc7J4rqlxXwv+lH9uRZE15+O6Ughp9SZp3EY+ZhuGw1lrnXz933OygL3qQblgs2KVrSHT4TSsQrKe1dDnET55JsPlODh//xfA/WVAS5pnKIYRtaSwVzMZTzmhT70DymfXsNWX72d+N8BDlt4wOLE9EgUXog7z8akdNoXwkcF5qYDRhsjEc5KIgScBN8l6O/9uB4v"
//  },
//  {
//    user = "cdtiwari",
//    id   = "8110",
//    sudo = "yes",
//    home = "/home/user/cdtiwari",
//    groups = [ "tsi" ],
//    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+UiGtQdbAjRagQGebeRBcjit51tw/fx3nz4TnCbeEVdJI6k2710tiU67HXOaX6aFN3XyPKlt+IUgVWLDjA2Qv5tQdZpLvEsX4uE5ZQGqXxOnOaxaO8mav4SjYy+vRAgFJzmnKxPA8lS+iREEgIIDQDjjV9TMmZu9YQ9bZX2lU+4K3nCWf//aenQ73yW+ZgvxYWyO9rMwvX/3zmlaLBMNa5bLd3mqR02gg+t+DJ673S2nSGV4lVLJjocwWUvEzBhlyIPaIBU82hN7oHURxQXHrISooaGkdEd3tv4HvvrhpVEXquvP0IxsP6rAmi3OpgbaO0f9wLBskJe3v5rs6Je7Y2H8P1XuDI1X1wU+dqRkmNKaOHjt/Hk/mZStFwxo60JJHEQuzvH3xmr3ZQlvrKBL7TRJMcLepda4CmDkZXUbr5wLwDZA912noGWp4Z32bXW7k8eocAbAROrgVls89cke5bHQ5cD/yMqln05uGYnLszcUGN4R8T4LXWJC2eeOn2V22kLumArQwNspxr0FGN/i24pzx/Pjd564mXhQWeeRYXAWGaiXIaBKL2Q14mSpJ5tVQNufHHnGctK/reLaajyYcbbn1agqaOABBJJDVeUUE8M1jpJN/q+Fchu8/JG7ADfJXrZyKJc0YiQm8e76E125/pdlK7PHeioWcKj+7gswlDw== cdtiwari@ebi.ac.uk"
//  },
//  {
//    user = "wildish",
//    id   = "5995",
//    sudo = "yes",
//    home = "/home/user/wildish",
//    groups = [ "tsi" ],
//    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDR6e6m/jUounQkAsMe9agxnB+X8HCgw/8R+JCe2QtonA0RDvGpoyd3fSwggloGUiRKi2tCShjJOz+D9FRRrSOy8nLlYNvi5AOrceHYqLzXsNWx+lhCo91Mx6p4l7cRqAj70aLEt4yXecBEUTffLwuyPTrYOWPWq7JeEHetYT0qVlEupVRHyAIvaeNzPi4SrzbC7f0xHEV9ydYlBd6AXOnQxindVggd9I94KeNwUiaWQXa40tkjTOlCq9BkGWbwh0udMlW5HZWkKYqa/o4sya/Ul247/qTLBaypU6LwG7DTrIY4PfF32sqDGg7hVR7f0KhF0Mct43PmYpa+MKnjwqZooCFWwxSA7c9hVzXyQVxirVTcT9rqJZ6QtSH5GL/8RfvKF9GBq5/zBw8wCiFh4ZUbrix33ESBBvfbbxVx8GcjsmnjyxP0nlYgaQw5T2kVew7c0AXqN8NXwXcaILRaeN0SZBxm3ZU9zadHa0mneXbqZDS/Fspwh6SYue8tysuKbBKpXMc+RUJRKmk07mLxxXHthHDpAj8TNZGjq/o5UAE3Wl+tsbARwxkWAsiewSnJlbMksB9iJbR/P+kDPNzjR6FN2faTftLcR70x42uKYJkPmH+mmKJgBuoovD9i44S++8nb0XTViTAVpX4o2W1XQfecTZthGkhz/HL3v4afxOITBw== wildish@ebi.ac.uk"
//  }
//]